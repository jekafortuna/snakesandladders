package ua.kyiv.snakesandladders.services.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ua.kyiv.snakesandladders.domain.ActionStatus;
import ua.kyiv.snakesandladders.domain.Person;
import ua.kyiv.snakesandladders.domain.Token;
import ua.kyiv.snakesandladders.services.DiceService;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MovingVerifierImplTest {

    @Mock
    private Person person;
    @Mock
    private Token token;
    @Mock
    private DiceService diceService;

    @InjectMocks
    private MovingVerifierImpl movingVerifier;

    @Test
    public void gameShouldCompleteByWinWhenReachingEndPosition() {
        int currentPosition = 96;
        int diceNumber = 4;
        when(person.getToken()).thenReturn(token);
        when(token.getPositionOnBoard()).thenReturn(currentPosition);
        when(diceService.rollDice()).thenReturn(diceNumber);

        ActionStatus actionStatus = movingVerifier.verifyAbilityToMove(person, diceNumber);
        assertEquals(ActionStatus.WINNER, actionStatus);
    }

    @Test
    public void gameShouldContinueWhenPlayerExceedsEndPosition() {
        int currentPosition = 96;
        int diceNumber = 5;
        when(person.getToken()).thenReturn(token);
        when(token.getPositionOnBoard()).thenReturn(currentPosition);
        when(diceService.rollDice()).thenReturn(diceNumber);

        ActionStatus actionStatus = movingVerifier.verifyAbilityToMove(person, diceNumber);
        assertEquals(ActionStatus.CANNOT_MOVE, actionStatus);
    }

    @Test
    public void gameShouldContinueWhenPlayerDoesNotExceedEndPosition() {
        int currentPosition = 6;
        int diceNumber = 5;
        when(person.getToken()).thenReturn(token);
        when(token.getPositionOnBoard()).thenReturn(currentPosition);
        when(diceService.rollDice()).thenReturn(diceNumber);

        ActionStatus actionStatus = movingVerifier.verifyAbilityToMove(person, diceNumber);
        assertEquals(ActionStatus.CAN_MOVE, actionStatus);
    }
}