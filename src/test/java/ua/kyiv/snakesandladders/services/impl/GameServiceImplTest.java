package ua.kyiv.snakesandladders.services.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ua.kyiv.snakesandladders.domain.ActionStatus;
import ua.kyiv.snakesandladders.domain.Person;
import ua.kyiv.snakesandladders.domain.Token;
import ua.kyiv.snakesandladders.services.DiceService;
import ua.kyiv.snakesandladders.services.MovingVerifier;
import ua.kyiv.snakesandladders.services.PersonCreationService;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GameServiceImplTest {

    @Mock
    private DiceService diceService;
    @Mock
    private PersonCreationService personCreationService;
    @Mock
    private MovingVerifier movingVerifier;
    @Mock
    private Person person;

    private Token token = new Token();
    @InjectMocks
    private GameServiceImpl gameService;

    @Before
    public void setUp() {
        when(personCreationService.addNewPlayer()).thenReturn(person);
        when(person.getToken()).thenReturn(token);
        person.setToken(token);
        token.setPositionOnBoard(1);
    }

    @Test
    public void shouldReturnStartedPosition() {
        Person person = gameService.createPlayer();
        assertEquals(1, person.getToken().getPositionOnBoard());
    }

    @Test
    public void shouldWinGameWhenReachFinalSquare() {
        when(movingVerifier.verifyAbilityToMove(any(), anyInt())).thenReturn(ActionStatus.WINNER);
        ActionStatus actionStatus = movingVerifier.verifyAbilityToMove(person, 5);
        assertEquals(ActionStatus.WINNER, actionStatus);
    }

    @Test
    public void shouldReachFourthPositionWhenCompletedThreeSteps() {
        when(diceService.rollDice()).thenReturn(3);
        when(movingVerifier.verifyAbilityToMove(any(), anyInt())).thenReturn(ActionStatus.CAN_MOVE);
        gameService.playGame(person);
        int result = person.getToken().getPositionOnBoard();

        assertEquals(4, result);
    }

    @Test
    public void shouldReachEighthPosition() {
        when(diceService.rollDice()).thenReturn(3, 4);
        when(movingVerifier.verifyAbilityToMove(any(), anyInt())).thenReturn(ActionStatus.CAN_MOVE);
        gameService.playGame(person);
        gameService.playGame(person);
        int result = person.getToken().getPositionOnBoard();

        assertEquals(8, result);
    }

    @Test
    public void shouldMoveFourStepsWhenDiceNumberIsFour() {
        when(diceService.rollDice()).thenReturn(4);
        when(movingVerifier.verifyAbilityToMove(any(), anyInt())).thenReturn(ActionStatus.CAN_MOVE);
        gameService.playGame(person);
        int result = person.getToken().getPositionOnBoard();

        assertEquals(5, result);
    }
}
