package ua.kyiv.snakesandladders.services.impl;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.assertj.core.api.Assertions.assertThat;

class DiceServiceImplTest {

    private static final int ZERO = 0;
    private static final int DICE_SIZE = 6;
    private static final int MIN_DICE_OUTPUT = 1;

    @InjectMocks
    private DiceServiceImpl diceService = new DiceServiceImpl();

    @Test
    void shouldReturnPositiveRollingDiceResult() {
        assertThat(diceService.rollDice()).isGreaterThan(ZERO);
    }

    @Test
    void shouldReturnRollingDiceResultNotExceededDiceSize() {
        assertThat(diceService.rollDice()).isLessThanOrEqualTo(DICE_SIZE);
    }

    @Test
    void shouldReturnRollingDiceResultNotLessThanMinDiceNumber() {
        assertThat(diceService.rollDice()).isGreaterThanOrEqualTo(MIN_DICE_OUTPUT);
    }
}