package ua.kyiv.snakesandladders.services;

import ua.kyiv.snakesandladders.domain.ActionStatus;
import ua.kyiv.snakesandladders.domain.Person;

public interface MovingVerifier {
    ActionStatus verifyAbilityToMove(Person person, int quantityOfSteps);
}
