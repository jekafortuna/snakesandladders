package ua.kyiv.snakesandladders.services;

public interface DiceService {
    int DICE_SIZE = 6;
    int MIN_DICE_OUTPUT = 1;

    int rollDice();
}
