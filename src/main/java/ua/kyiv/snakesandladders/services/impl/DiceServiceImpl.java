package ua.kyiv.snakesandladders.services.impl;

import org.springframework.stereotype.Service;
import ua.kyiv.snakesandladders.services.DiceService;

import java.util.Random;

@Service
public class DiceServiceImpl implements DiceService {
    @Override
    public int rollDice() {
        return new Random().nextInt(DICE_SIZE) + MIN_DICE_OUTPUT;
    }
}
