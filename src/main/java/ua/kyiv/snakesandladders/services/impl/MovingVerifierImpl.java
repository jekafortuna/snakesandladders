package ua.kyiv.snakesandladders.services.impl;

import org.springframework.stereotype.Service;
import ua.kyiv.snakesandladders.domain.ActionStatus;
import ua.kyiv.snakesandladders.domain.Person;
import ua.kyiv.snakesandladders.services.MovingVerifier;

import static ua.kyiv.snakesandladders.services.impl.GameServiceImpl.END_POSITION;

@Service
public class MovingVerifierImpl implements MovingVerifier {
    @Override
    public ActionStatus verifyAbilityToMove(Person person, int quantityOfSteps) {
        ActionStatus actionStatus = ActionStatus.CANNOT_MOVE;
        int currentPosition = person.getToken().getPositionOnBoard();
        if (currentPosition + quantityOfSteps == END_POSITION) {
            actionStatus = ActionStatus.WINNER;
        } else if (currentPosition + quantityOfSteps < END_POSITION) {
            actionStatus = ActionStatus.CAN_MOVE;
        }
        return actionStatus;
    }
}
