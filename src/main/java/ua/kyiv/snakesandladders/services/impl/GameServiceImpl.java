package ua.kyiv.snakesandladders.services.impl;

import org.springframework.stereotype.Service;
import ua.kyiv.snakesandladders.domain.ActionStatus;
import ua.kyiv.snakesandladders.domain.Person;
import ua.kyiv.snakesandladders.services.DiceService;
import ua.kyiv.snakesandladders.services.GameService;
import ua.kyiv.snakesandladders.services.MovingVerifier;
import ua.kyiv.snakesandladders.services.PersonCreationService;

@Service
public class GameServiceImpl implements GameService {

    public static final int START_POSITION = 1;
    public static final int END_POSITION = 100;

    private DiceService diceService;

    private MovingVerifier movingVerifier;

    private PersonCreationService personCreationService;

    public Person createPlayer() {
        return personCreationService.addNewPlayer();
    }

    @Override
    public Person playGame(Person person) {
        int diceNumber = diceService.rollDice();
        ActionStatus actionStatus = movingVerifier.verifyAbilityToMove(person, diceNumber);
        if (!actionStatus.equals(ActionStatus.CANNOT_MOVE)) {
            provideMovingThroughTheBoard(person, diceNumber);
        }
        return person;
    }

    private void provideMovingThroughTheBoard(Person person, int quantityOfSteps) {
        person.getToken().setPositionOnBoard(person.getToken().getPositionOnBoard() + quantityOfSteps);
    }
}
