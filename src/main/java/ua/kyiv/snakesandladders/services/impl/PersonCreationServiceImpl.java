package ua.kyiv.snakesandladders.services.impl;

import org.springframework.stereotype.Service;
import ua.kyiv.snakesandladders.domain.Person;
import ua.kyiv.snakesandladders.services.PersonCreationService;

import static ua.kyiv.snakesandladders.services.impl.GameServiceImpl.START_POSITION;

@Service
public class PersonCreationServiceImpl implements PersonCreationService {
    @Override
    public Person addNewPlayer() {
        Person newPlayer = new Person();
        newPlayer.getToken().setPositionOnBoard(START_POSITION);
        return newPlayer;
    }
}
