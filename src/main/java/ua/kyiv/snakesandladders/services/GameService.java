package ua.kyiv.snakesandladders.services;

import ua.kyiv.snakesandladders.domain.Person;

public interface GameService {
    Person playGame(Person person);
}
