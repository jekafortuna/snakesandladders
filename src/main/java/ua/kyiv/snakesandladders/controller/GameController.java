package ua.kyiv.snakesandladders.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.kyiv.snakesandladders.domain.Person;
import ua.kyiv.snakesandladders.domain.PersonAttributes;
import ua.kyiv.snakesandladders.services.GameService;

@RestController
@RequestMapping(value = "/game")
public class GameController {

    private GameService gameService;

    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @PostMapping("/player")
    public PersonAttributes simulationOfMoving(@RequestBody PersonAttributes player){
        Person person = player.getInnerInfo();
        return new PersonAttributes(gameService.playGame(person));
    }
}
