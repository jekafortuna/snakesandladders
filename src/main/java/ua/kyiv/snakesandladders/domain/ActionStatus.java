package ua.kyiv.snakesandladders.domain;

public enum ActionStatus {
    WINNER, CAN_MOVE, CANNOT_MOVE
}
