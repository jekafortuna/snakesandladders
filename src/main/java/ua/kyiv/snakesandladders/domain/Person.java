package ua.kyiv.snakesandladders.domain;

public class Person {
    private Token token;

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }
}
