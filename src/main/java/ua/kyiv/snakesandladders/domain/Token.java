package ua.kyiv.snakesandladders.domain;

public class Token {
    private int positionOnBoard;

    public int getPositionOnBoard() {
        return positionOnBoard;
    }

    public void setPositionOnBoard(int positionOnBoard) {
        this.positionOnBoard = positionOnBoard;
    }
}
