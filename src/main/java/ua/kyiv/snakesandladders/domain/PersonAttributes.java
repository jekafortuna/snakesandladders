package ua.kyiv.snakesandladders.domain;

public class PersonAttributes {
    private Person innerInfo;

    public PersonAttributes(Person innerInfo) {
        this.innerInfo = innerInfo;
    }

    public Person getInnerInfo() {
        return innerInfo;
    }

    public void setInnerInfo(Person innerInfo) {
        this.innerInfo = innerInfo;
    }
}
