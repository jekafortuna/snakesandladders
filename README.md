# SnakesAndLadders

### Snakes and Ladders Kata
http://agilekatas.co.uk/katas/SnakesAndLadders-Kata


Provided implementation of Feature 1:  
Moving a token across the board using dice rolls. 
There is an ability to roll a dice, move token the number of squares indicated by the dice roll
and to win if to land on the final square. 
